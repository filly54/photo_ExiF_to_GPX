#!/usr/bin/env python3

from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
import gpxpy
import gpxpy.gpx
import glob
import os
from datetime import datetime
import argparse

def get_exif_data(image):
    """Returns a dictionary from the exif data of an PIL Image item. Also converts the GPS Tags"""
    exif_data = {}
    info = image._getexif()
    if info:
        for tag, value in info.items():
            decoded = TAGS[tag]
            if decoded == "GPSInfo":
                gps_data = {}
                for t in value:
                    sub_decoded = GPSTAGS[t]
                    gps_data[sub_decoded] = value[t]
                exif_data[decoded] = gps_data
                
            if decoded == "DateTime":
                exif_data[decoded] = value

    return exif_data

def _get_if_exist(data, key):
    if key in data:
        return data[key]
		
    return None
	
def _convert_to_degress(value):
    """Helper function to convert the GPS coordinates stored in the EXIF to degress in float format"""
    d0 = value[0][0]
    d1 = value[0][1]
    d = float(d0) / float(d1)

    m0 = value[1][0]
    m1 = value[1][1]
    m = float(m0) / float(m1)

    s0 = value[2][0]
    s1 = value[2][1]
    s = float(s0) / float(s1)

    return d + (m / 60.0) + (s / 3600.0)

def get_lat_lon(exif_data):
    """Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)"""
    lat = None
    lon = None

    if "GPSInfo" in exif_data:		
        gps_info = exif_data["GPSInfo"]

        print("lat and long")
        gps_latitude = _get_if_exist(gps_info, "GPSLatitude")
        print (gps_latitude)
        gps_latitude_ref = _get_if_exist(gps_info, 'GPSLatitudeRef')
        gps_longitude = _get_if_exist(gps_info, 'GPSLongitude')
        print (gps_longitude)
        gps_longitude_ref = _get_if_exist(gps_info, 'GPSLongitudeRef')

        if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
            lat = _convert_to_degress(gps_latitude)
            print (lat)
            if gps_latitude_ref != "N":                     
                lat = 0 - lat

            lon = _convert_to_degress(gps_longitude)
            print (lon)
            if gps_longitude_ref != "E":
                lon = 0 - lon

    return lat, lon

def parseArguments():
    parser = argparse.ArgumentParser(description='Convert geotagged pictures to xml file')
    parser.add_argument('path', help= 'path to geotagged pictures')
    parser.add_argument('outputFile', help= 'output gpx file')
    args = parser.parse_args()
    print(args)

    return args


def add_GPS_point(gpsData):
    dateTime = exif_data["DateTime"]
    # convert exif dateTime to dateTime type
    dateTime = datetime.strptime(dateTime, '%Y:%m:%d %H:%M:%S')
    # append a gpx waypoint with lat,log,dateTime to gpx
    gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(gpsData[0], gpsData[1], time=dateTime))
    return gpx_segment

if __name__ == "__main__":

    args = parseArguments()
    
    os.chdir(args.path) # to change directory to argument passed for '--path'
    
    print (os.getcwd())
    
    gpx = gpxpy.gpx.GPX()

    # Create first track in our GPX:
    gpx_track = gpxpy.gpx.GPXTrack()
    gpx.tracks.append(gpx_track)

    # Create first segment in our GPX track:
    gpx_segment = gpxpy.gpx.GPXTrackSegment()
    gpx_track.segments.append(gpx_segment)

    # search for all jpg files in current work direction (recursive)
    files = glob.glob(args.path + '/**/*.jpg', recursive=True)
    # sort files
    files = sorted(files)
    print("Files found: " + str(len(files)))

    fileGpsNo = 0
    for filename in files:
        image = Image.open(filename)
        # extract exif infos form file
        exif_data = get_exif_data(image)
        image.close()
        # convert latitude and logitude in degrees(float)
        gpsData = get_lat_lon(exif_data)

        # check if gpsData valid
        if (gpsData[0]):
            fileGpsNo = fileGpsNo + 1
            gps_segment = add_GPS_point(gpsData)

    print("Files with GPS Data found: " + str(fileGpsNo))
    # write gpx info to gpx in xml format
    gpx_file = open(args.outputFile + '.gpx', 'w')
    gpx_file.write(gpx.to_xml())
    gpx_file.close()
