import unittest
from src.gpx import _convert_to_degress

class TestExifToGpx(unittest.TestCase):
    def test_convert_to_degress(self):
        value = ((44, 1), (24, 1), (562280, 10000))
        self.assertEqual(_convert_to_degress(value), 44.415618888888886)

        value = ((110, 1), (34, 1), (157891, 10000))
        self.assertEqual(_convert_to_degress(value), 110.57105252777778)
